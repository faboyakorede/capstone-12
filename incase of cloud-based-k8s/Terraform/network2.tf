#Create Vpc 
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "Altschool-VPC"
 }
}


# create public subnet 1 
resource "aws_subnet" "public-1-subnet" {
 vpc_id     = aws_vpc.main.id
 cidr_block = var.public1_cidr
 availability_zone = "eu-west-1b"
 tags = {
  Name = "Public Subnet-1"
 }
}

# create public subnet 2
resource "aws_subnet" "public-2-subnet"{
  vpc_id     = aws_vpc.main.id
  cidr_block = var.public2_cidr

  tags = {
    Name = "public subnet-2"
  }
}

# Create Igw
resource "aws_internet_gateway" "altschool_ig" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "Altschool Internet Gateway"
  }
}


# create route table
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.altschool_ig.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.altschool_ig.id
  }

  tags = {
    Name = "Public Route Table"
  }
}

# route table association
resource "aws_route_table_association" "pub1" {
  subnet_id      = aws_subnet.public-1-subnet.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "pub2" {
  subnet_id      = aws_subnet.public-2-subnet.id
  route_table_id = aws_route_table.public_rt.id
}
variable "public1_cidr" {
    type = string
    default = "10.0.1.0/24"
}

variable "public2_cidr" {
    type = string
    default = "10.0.2.0/24"
}
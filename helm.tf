#Helm repo to set up monitoring, prometheus, grafana
resource "kubernetes_namespace" "monitoring" {
  depends_on = [aws_eks_node_group.Eks-node_group]
  metadata {
    name = var.namespace
  }
}

resource "helm_release" "kube-prometheus" {
  depends_on = [kubernetes_namespace.monitoring]
  namespace  = var.namespace
  repository = "https://prometheus-community.github.io/helm-charts"
  version    = "45.7.1"
  chart      = "kube-prometheus-stack"
  timeout    = 2000
  name       = "prometheus"
}


resource "kubernetes_namespace" "ingress" {
  depends_on = [aws_eks_node_group.Eks-node_group]
  metadata {
    name = "ingress"
  }
}

#helm repo to set up nginx-ingress 
resource "helm_release" "nginix-ingress" {
  depends_on = [kubernetes_namespace.ingress]
  name       = "nginix-ingress"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  namespace  = "ingress"
  version    = "4.5.2"
}

resource "null_resource" "get_nlb_hostname" {
  depends_on = [helm_release.nginix-ingress]
  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --name Alt-eks --region us-east-1 && kubectl get svc nginix-ingress-ingress-nginx-controller  --namespace ingress -o jsonpath='{.status.loadBalancer.ingress[*].hostname}' > ${path.module}/lb_hostname.txt"
  }
}



# Define the Kubernetes Deployment
resource "kubernetes_deployment" "my_app" {
  depends_on =[helm_release.nginix-ingress]
  metadata {
    name = "my-app-deployment"
    labels = {
      app = "my-app"
    }
  }

  spec {
    replicas = 3  # Number of desired replicas

    selector {
      match_labels = {
        app = "my-app"
      }
    }

    template {
      metadata {
        labels = {
          app = "my-app"
        }
      }

      spec {
        container {
          name  = "my-app-container"
          image = "saluteslim/capstone:1.0"  # Replace with your actual application image

          port {
            container_port = 8080  # Port on which your application listens
          }
          env {
            name  = "DB_HOST"
            value = "mongo-service"
          }
          env {
            name  = "DB_PORT"
            value = "27107"
          }
          env {
            name  = "DB_USER"
            value = "admin"
          }
          env {
            name  = "DB_PASS"
            value = "password"
          }
          env {
            name  = "DB_NAME"
            value = "admin"
          }
        }
         restart_policy = "Always"
      }
    }
  }
}

# Define the Kubernetes Service
resource "kubernetes_service" "my_app_service" {
  depends_on=[kubernetes_deployment.my_app]
  metadata {
    name = "my-app-service"
  }

  spec {
    selector = {
      app = "my-app"
    }

    port {
      port        = 80
      target_port = 8080
    }

    type = "LoadBalancer"  # Use "LoadBalancer" if you want to expose the service publicly
  }
}


# Define the MongoDB Deployment
resource "kubernetes_deployment" "mongodb" {
  depends_on =[kubernetes_deployment.my_app]
  metadata {
    name = "mongo-db"
    labels = {
      app = "mongo-db"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "mongo-db"
      }
    }

    template {
      metadata {
        labels = {
          app = "mongo-db"
        }
      }

      spec {
        container {
          name  = "mongodb-container"
          image = "mongo:latest"

          port {
            container_port = 27017
          }

          env {
            name  = "MONGO_INITDB_ROOT_USERNAME"
            value = "admin"
          }

          env {
            name  = "MONGO_INITDB_ROOT_PASSWORD"
            value = "password123"
          }

          env {
            name  = "MONGO_INITDB_DATABASE"
            value = "admin"
          }
          env {
            name  = "MONGODB_URI"
            value = "mongodb://mongo-db:27107"
          }
        }
         restart_policy = "Always"
      }
    }
  }
}


resource "kubernetes_service" "mong-service" {
  depends_on =[kubernetes_deployment.mongodb]
  metadata {
    name = "mongo-service"
  }

  spec {
    selector = {
      app = "mongo-db"
    }

    port {
      protocol = "TCP"
      port     = 27107  # Port on which your application listens
      target_port = 27107  # Port on which your application container listens
    }

    type = "NodePort"  # Change the service type as per your requirements
  }
}

